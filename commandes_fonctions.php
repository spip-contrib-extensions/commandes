<?php

/**
 * Fonctions du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Un fitre que l'on peut utiliser en argument d'une inclusion si on veut etre sur que l'affichage change quand la commande change
 * <INCLURE{fond=macommande,id_commande,hash=#ID_COMMANDE|commandes_hash} />
 *
 * @param int $id_commande
 * @return string
 */
function filtre_commandes_hash_dist($id_commande) {

	return md5(serialize(sql_allfetsel('id_commandes_detail,prix_unitaire_ht,taxe,objet,id_objet,quantite', 'spip_commandes_details', 'id_commande=' . intval($id_commande))));
}

/**
 * @deprecated  v2.4.0
 */
function commandes_hash($id_commande) {return filtre_commandes_hash_dist($id_commande);}


/**
 * Retourne les différents statuts possibles pour une commande ou le nom d'un statut précis
 *
 * @filtre
 *
 * @param bool|string $statut
 *     - vide ou false pour retourner un tableau
 *     - nom d'un statut précis pour retourner sa chaîne de langue
 * @return array|string
 *     - array: tableau associatif des statuts possibles et leurs chaînes de langue
 *     - string: chaîne de langue d'un statut
 **/
function commandes_lister_statuts($statut = false) {


	// retourne les statuts déclarés dans declarer_tables_objets_sql
	if (!function_exists('objet_info')) {
		include_spip('inc/filtres');
	}
	$statuts = array_map('_T', objet_info('commande', 'statut_textes_instituer'));
	if ($statut and $nom = $statuts[$statut]) {
		return $nom;
	}
	if ($statut) {
		return $statut;
	} else {
		return $statuts;
	}
}

/**
 * Une fonction pour memoriser les taxes par taux et retourner le tableau de detail en recap
 * @filtre
 *
 * @param null|float $prix_ht
 * @param null|float $prix_ttc
 * @param null|float $taxe
 * @return array|string
 */
function filtre_commande_totalise_taxes_dist($prix_ht = null, $prix_ttc = null, $taxe = null) {

	static $taxes = [];
	if (is_null($prix_ht) or !strlen($prix_ht)) {
		// par defaut on reset le tableau mais si on a a besoin plusieurs fois,
		$return = $taxes;
		// possible de le garder en passant n'importe quoi non vide en second argument
		if (is_null($prix_ttc) or !strlen($prix_ttc)) {
			$taxes = [];
		}
		return $return;
	}

	if (
		$prix_ht
		and $prix_ttc
		and $prix_ht = floatval(str_replace(',', '.', $prix_ht))
		and $prix_ttc = floatval(str_replace(',', '.', $prix_ttc))
		and (floatval($prix_ttc) - floatval($prix_ht)) > 0.001
	) {
		$taux = $taxe ? (string)round(floatval($taxe) * 100, 1) : (string)round((floatval($prix_ttc) / floatval($prix_ht) - 1.0) * 100, 1);
		if (!isset($taxes[$taux])) {
			$taxes[$taux] = 0;
		}
		$taxes[$taux] += ($prix_ttc - $prix_ht);
	}

	return '';
}
/**
 * @deprecated  v2.4.0
 */
function commande_totalise_taxes($prix_ht = null, $prix_ttc = null) {return filtre_commande_totalise_taxes_dist($prix_ht, $prix_ttc);}


/**
 * Filtre pour utiliser la fonction d'arrondi des quantite
 * @filtre
 *
 * @param int|float $quantite
 * @param string $objet
 * @param int $id_objet
 * @return int|float
 */
function filtre_commandes_arrondir_quantite_dist($quantite, $objet = '', $id_objet = 0) {

	$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
	return $commande_arrondir_quantite($quantite, $objet, $id_objet);
}
/**
 * @deprecated  v2.4.0
 */
function commandes_arrondir_quantite($quantite, $objet = '', $id_objet = 0) {return filtre_commandes_arrondir_quantite_dist($quantite, $objet, $id_objet);}


/**
 * Afficher la quantite si differente de 1
 * @filtre
 *
 * @param int|float $quantite
 * @param string $objet
 * @param int $id_objet
 * @return string
 */
function filtre_commandes_afficher_quantite_descriptif_dist($quantite, $objet = '', $id_objet = 0) {

	if ($quantite != 1) {
		$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
		return $commande_arrondir_quantite($quantite, $objet, $id_objet) . ' &times; ';
	}
	return '';
}
/**
 * @deprecated  v2.4.0
 */
function commandes_afficher_quantite_descriptif($quantite, $objet = '', $id_objet = 0) {return filtre_commandes_afficher_quantite_descriptif_dist($quantite, $objet, $id_objet);}

/**
 * Afficher la quantite, en arrondissant eventuellement
 * (par defaut fait juste l'arrondi int natif)
 * @filtre
 *
 * @param int|float $quantite
 * @param string $objet
 * @param int $id_objet
 * @return string
 */
function filtre_commandes_afficher_quantite_dist($quantite, $objet = '', $id_objet = 0) {

	$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
	return $commande_arrondir_quantite($quantite, $objet, $id_objet);
}
/**
 * @deprecated  v2.4.0
 */
function commandes_afficher_quantite($quantite, $objet = '', $id_objet = 0) {return filtre_commandes_afficher_quantite_dist($quantite, $objet, $id_objet);}


/**
 * @filtre
 * @param $prix_ttc
 * @param $quantite
 * @param $prix_unit_ht
 * @param $reduction
 * @param $taxe
 * @return mixed|string
 */
function filtre_commandes_afficher_prix_detaille_abbr_dist($prix_ttc, $quantite, $prix_unit_ht, $reduction, $taxe) {


	// cas facile, aucun calul a detailler
	if ($quantite == 1 and $reduction == 0.0 and $taxe == 0.0) {
		return $prix_ttc;
	}

	$abbr = prix_formater($prix_unit_ht);
	$parentheses = false;
	if ($reduction > 0.0) {
		$abbr = "$abbr - " . round($reduction * 100, 2) . "% &times; $abbr";
		$prix_unit_ht = $prix_unit_ht * (1.0 - $reduction);
		if ($reduction > 1.00) {
			$prix_unit_ht = 0;
		}
		$parentheses = true;
	}
	if ($taxe) {
		if ($parentheses) {
			$abbr = "($abbr)";
		}
		$taxe_unit = $prix_unit_ht * $taxe;
		$abbr = "$abbr + " . prix_formater($taxe_unit);
		$parentheses = true;
	}

	if ($quantite != 1) {
		if ($parentheses) {
			$abbr = "($abbr)";
		}
		$abbr = "{$quantite} &times; $abbr";
	}


	return '<abbr title="= ' . attribut_html($abbr) . '">' . $prix_ttc . '</abbr>';
}
/**
 * @deprecated  v2.4.0
 */
function commandes_afficher_prix_detaille_abbr($prix_ttc, $quantite, $prix_unit_ht, $reduction, $taxe) {return filtre_commandes_afficher_prix_detaille_abbr_dist($prix_ttc, $quantite, $prix_unit_ht, $reduction, $taxe);}


function filtre_commandes_afficher_reduction_si_dist($reduction) {

	if ($reduction <= 0.0) {
		return '';
	}
	return min(100, round($reduction * 100, 2)) . '%';
}
/**
 * @deprecated  v2.4.0
 */
function commandes_afficher_reduction_si($reduction) {return filtre_commandes_afficher_reduction_si_dist($reduction);}

/**
 * Critère pour prendre la commande en cours du visiteur, qu'il soit connecté ou non
 *
 * @param string $idb
 * @param object $boucles
 * @param object $crit
 * @example <BOUCLE_commande(COMMANDES) {encours_visiteur}>
 *
 * @uses commandes_calculer_critere_encours_visiteur()
 */
function critere_COMMANDES_encours_visiteur_dist($idb, &$boucles, $crit) {

	$boucle = &$boucles[$idb];
	$cond = $crit->cond;
	$not = $crit->not ? 'NOT ' : '';
	$where = "'$not" . $boucle->id_table . ".id_commande = '.commandes_id_commande_encours_visiteur()";
	$boucle->where[] = $where;
	// drapeau pour avoir un cache visiteur
	$boucles[$idb]->descr['session'] = true;
}

/**
 * Retourne le numéro de la commande en cours du visiteur, qu'il soit connecté ou non
 *
 * Soit la commande est en session, soit on prend celle dans la db.
 * Nb : il ne peut en théorie y avoir qu'une seule commande en cours par auteur,
 * dans le cas improbable où il y en aurait plusieurs, on prend la plus récente.
 *
 * @return int
 *     Numéro de la commande ou 0 s'il n'y en a pas
 */
function commandes_id_commande_encours_visiteur() {

	include_spip('inc/session');
	$id_commande = 0;
	// Soit la commande est dans la session, que le visiteur soit connecté ou pas
	// On vérifie le statut au cas-où, même si c'est forcément "encours" normalement
	if (
		!$id_commande = sql_getfetsel('id_commande', 'spip_commandes', [
			'statut = ' . sql_quote('encours'),
			'id_commande = ' . intval(session_get('id_commande')),
		])
		and $id_auteur = session_get('id_auteur')
		// Soit on prend la plus récente "encours" de l'auteur connecté
	) {
		$id_commande = sql_getfetsel('id_commande', 'spip_commandes', [
			'statut = ' . sql_quote('encours'),
			'id_auteur = ' . intval($id_auteur),
		], '', 'date DESC');
	}
	$id_commande = intval($id_commande);
	return $id_commande;
}

/**
 * La fonction peut-être appellee par la fonction générique generer_objet_info($id_commande, 'commmande', 'infos_client')
 * dans ce cas $ligne_sql_commande est fournie en second argument mais n'est pas utile ici
 * et $cle vaut 'infos_client'
 *
 * @param int $id_commande
 * @param array $ligne_sql
 *   n'est pas utilisée
 * @param string $cle
 * @return array|mixed
 */
function generer_commande_infos_client($id_commande, $ligne_sql_commande = [], $cle = '') {
	include_spip('inc/commandes');

	$commande_infos_client = charger_fonction('infos_client', 'commande');
	$infos = $commande_infos_client($id_commande);

	// On peut demander un clé précise
	if ($cle) {
		if (strpos($cle, 'infos_client/') === 0 or $cle === 'infos_client') {
			$cle = substr($cle, 12);
			$cle = ltrim($cle, '/');
		}
		if ($cle) {
			$infos = table_valeur($infos, $cle);
		}
	}

	return $infos;
}

/**
 * Filtre helper pour utiliser les infos clients dans un squelette
 * @param int $id_commande
 * @param string $cle
 * @return array|mixed
 */
function filtre_generer_commande_infos_client_dist($id_commande, $cle) {
	return generer_commande_infos_client($id_commande, [], $cle);
}
