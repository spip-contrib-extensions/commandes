<?php

/**
 * Fonctions du plugin Commandes relatives à la référence de commande
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Commandes
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}


/**
 * Informations importantes d'une commande dans un tableau normé, extensible dans un pipeline
 *
 * Afin de garder une compatibilité avec les anciens comportements,la fonction recherche déjà les informations en plusieurs endroits, notamment pour les coordonnées.
 *
 * @param int $id_commande
 *   Identifiant de la commande
 * @param bool $forcer_calcul
 *   Si vrai, force la recherche de toutes les infos même si ça avait déjà été fait dans le même hit
 * @return array
 *   Retourne un tableau avec les clés possibles suivantes, optionnelles suivant ce qui est trouvé :
 *   - int id_auteur : compte utilisateur de la personne qui a fait la commande
 *   - string nom : nom complet de la personne ou orga qui a fait la commande
 *   - array livraison
 *     - string nom : nom complet de la personne à qui livrer
 *     - string organisation : nom complet de l'organisation à qui livrer
 *     - array adresse
 *       - string voie
 *       - string complement
 *       - string code_postal
 *       - string ville
 *       - string localite_dependante
 *       - string zone_administrative
 *       - string pays : code international
 *     - string telephone : numero de téléphone pour la livraison
 *   - array facturation
 *     - string nom : nom complet de la personne à qui facturer
 *     - string organisation : nom complet de l'organisation à qui facturer
 *     - array adresse
 *       - string voie
 *       - string complement
 *       - string code_postal
 *       - string ville
 *       - string localite_dependante
 *       - string zone_administrative
 *       - string pays : code international
 *     - string no_tva_intra : numero de TVA intracommunautaire utilisé pour la facturation
 *     - string telephone : numero de téléphone pour la facturation (contact administratif)
 */
function commande_infos_client_dist($id_commande, $forcer_calcul = false) {
	static $infos = [];
	$id_commande = intval($id_commande);

	if (!$id_commande) {
		return [];
	}

	if ($forcer_calcul or !isset($infos[$id_commande])) {
		include_spip('action/editer_liens');

		$commande = sql_fetsel('*', 'spip_commandes', 'id_commande = ' . intval($id_commande));
		$infos[$id_commande]['id_auteur'] = $id_objet_nom = intval($commande['id_auteur']);
		$objet_nom = 'auteur';

		// Prendre en compte C&O
		if (test_plugin_actif('contacts')) {
			if ($organisation = sql_fetsel('*', 'spip_organisations', 'id_auteur = ' . $infos[$id_commande]['id_auteur'])) {
				$id_objet_nom = $id_organisation = intval($organisation['id_organisation']);
				$objet_nom = 'organisation';
				// On ajoute l'orga complète pour qui en aurait besoin en plus sans refaire de requête
				$infos[$id_commande]['organisation'] = $organisation;
			}
			elseif ($contact = sql_fetsel('*', 'spip_contacts', 'id_auteur = ' . $infos[$id_commande]['id_auteur'])) {
				$id_objet_nom = $id_contact = intval($contact['id_contact']);
				$objet_nom = 'contact';
				// On ajoute le contact complet pour qui en aurait besoin en plus sans refaire de requête
				$infos[$id_commande]['contact'] = $contact;
			}
		}

		// Calcul du nom
		include_spip('inc/filtres');
		$infos[$id_commande]['nom'] = generer_info_entite($id_objet_nom, $objet_nom, 'titre');

		// Calcul des deux adresses possibles attendues
		if (test_plugin_actif('coordonnees')) {
			foreach (['facturation', 'livraison'] as $type) {
				// On cherche directement lié à la commande avec le bon type
				if ($liens = objet_trouver_liens(['adresse' => '*'], ['commande' => $id_commande], ['type = ' . sql_quote($type)])) {
					$infos[$id_commande][$type] = [];
					$infos[$id_commande][$type]['adresse'] = sql_fetsel('*', 'spip_adresses', 'id_adresse = ' . $liens[0]['id_adresse']);
					if (!isset($infos[$id_commande][$type]['nom'])) {
						$infos[$id_commande][$type]['nom'] = ($objet_nom === 'organisation' ? '' : $infos[$id_commande]['nom']);
					}
					if (!isset($infos[$id_commande][$type]['organisation'])) {
						$infos[$id_commande][$type]['organisation'] = ($objet_nom === 'organisation' ? $infos[$id_commande]['nom'] : '');
					}
					$infos[$id_commande][$type]['telephone'] = '';
					if ($liens = objet_trouver_liens(['numero' => '*'], ['commande' => $id_commande], ['type = ' . sql_quote($type)])) {
						$telephones = sql_allfetsel('numero', 'spip_numeros', sql_in('id_numero', array_column($liens, 'id_numero')));
						$telephones = array_column($telephones, 'numero');
						$telephones = array_filter($telephones);
						if (!empty($telephones)) {
							$infos[$id_commande][$type]['telephone'] = reset($telephones);
						}
					}
				}
			}
		}

		if (!empty($infos[$id_commande]['facturation'])) {
			// provisionner le no_tva_intra
			$infos[$id_commande]['facturation']['no_tva_intra'] = '';
		}

		$infos[$id_commande] = pipeline(
			'commande_infos_client',
			[
				'args' => ['id_commande' => $id_commande, 'commande' => $commande],
				'data' => $infos[$id_commande],
			]
		);

		// si on a une seule adresse, repercuter sur l'autre (adresse de livraison identique à la facturation, ou le contraire...)
		// on fait ça après le pipeline, pour laisser les plugins gérer l'absence d'une des adresses si besoin
		if (empty($infos[$id_commande]['facturation'])
			and !empty($infos[$id_commande]['livraison'])) {
			$infos[$id_commande]['facturation'] = $infos[$id_commande]['livraison'];
			// provisionner le no_tva_intra
			$infos[$id_commande]['facturation']['no_tva_intra'] = '';
		}
		elseif (empty($infos[$id_commande]['livraison'])
			and !empty($infos[$id_commande]['facturation'])) {
			$infos[$id_commande]['livraison'] = $infos[$id_commande]['facturation'];
			// supprimer le no_tva_intra
			unset($infos[$id_commande]['livraison']['no_tva_intra']);
		}

		// normaliser le code pays en ISO 3char
		foreach (['facturation', 'livraison'] as $type) {
			if (!empty($infos[$id_commande][$type]['adresse']['pays'])) {
				// si on a un code_pays sur 2 lettres ou numerique, le convertir en code ISO 3
				$infos[$id_commande][$type]['adresse']['pays'] = commandes_normalise_code_pays($infos[$id_commande][$type]['adresse']['pays']);
			}

		}

	}

	return $infos[$id_commande];
}


/**
 * Passer les code_pays en code alpha 3 si besoin
 * @param $code_pays
 * @return mixed|string
 */
function commandes_normalise_code_pays($code_pays) {
	static $code_conversion;
	$code_pays = trim($code_pays);
	if (strlen($code_pays) == 2 and test_plugin_actif('pays')) {
		if (is_null($code_conversion)) {
			$code_conversion = sql_allfetsel('code, code_alpha3', 'spip_pays');
			$code_conversion = array_column($code_conversion, 'code_alpha3', 'code');
		}
		if (isset($code_conversion[$code_pays])) {
			$code_pays = $code_conversion[$code_pays];
		}
	}
	return $code_pays;
}