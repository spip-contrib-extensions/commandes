<?php


/**
 * Calcul le contenu des notifications si une variante selon le statut/statut_ancien est disponible
 *
 * Cherche dans l'ordre un squelette
 * {$quoi}_{$statut}_depuis_{$statut_ancien}
 * {$quoi}_{$statut}
 * {$quoi}_depuis_{$statut_ancien}
 * {$quoi}
 * pour chaque partie ['', '_court', '_html']
 *
 * Si on en trouve un, on renseigne la valeur, sinon on laisse vide et notifications_envoyer() finira le traitement
 *
 * @see notifications_envoyer()
 *
 * @param string $quoi
 * @param int $id
 * @param array $options
 * @param array $destinataire
 * @param string $mode
 * @return array
 */
function commande_notification_contenu_dist($quoi, $id, $options, $destinataire, $mode, $contexte = null) {

	$variantes = [];
	if (!empty($options['statut'])) {
		if (!empty($options['statut_ancien'])) {
			$variantes[] = '_' . $options['statut'] . '_depuis_' . $options['statut_ancien'];
		}
		$variantes[] = '_' . $options['statut'];
	}
	if (!empty($options['statut_ancien'])) {
		$variantes[] = '_depuis_' . $options['statut_ancien'];
	}
	// et la variante de base comme dans notifications_envoyer()
	$variantes[] = '';

	// On construit le contexte utile
	if (is_null($contexte)) {
		$contexte = array(
			'quoi' => $quoi,
			'id' => $id,
			'options' => $options,
			'destinataire' => $destinataire,
			'mode' => $mode,
			'lang' => $GLOBALS['spip_lang'],
			'objet' => 'commande',
			'id_objet' => $id,
		);
	}

	$contenu = [];
	$parties = ['texte' => '', 'html' => '_html', 'court' => '_court'];
	foreach ($parties as $k => $suffixe) {
		foreach ($variantes as $variante) {
			if (trouver_fond($fond = "notifications/${quoi}" . $variante . $suffixe)){
				$contenu[$k] = trim(recuperer_fond($fond, $contexte));
				// si le texte est vide, c'est une notification vide qu'on ne veut pas envoyer,
				// pas la peine de chercher le reste
				if ($k === 'texte' and empty($contenu[$k])) {
					return $contenu;
				}
				break;
			}
		}
	}

	return $contenu;
}
