<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/commandes?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abbr_hors_taxe' => 'sem IVA',
	'abbr_prix_unitaire' => 'P.U.',
	'abbr_quantite' => 'Qtd',
	'abbr_total_ht' => 'Total sem IVA',
	'abbr_toutes_taxes_comprises' => 'com IVA',
	'action_modifier' => 'Modificar',
	'action_supprimer' => 'Remover',

	// B
	'bonjour' => 'Bom dia',

	// C
	'commande_client' => 'Assinante',
	'commande_creer' => 'Criar um pedido',
	'commande_date' => 'Data',
	'commande_date_paiement' => 'Data de pagamento',
	'commande_echeances_date_debut' => 'Data de início dos pagamentos periódicos:',
	'commande_echeances_paiements_infini' => 'Pagamentos seguintes',
	'commande_echeances_paiements_previsions' => 'Pagamentos previsionais',
	'commande_echeances_paiements_tous' => 'Todos os pagamentos',
	'commande_echeances_type' => 'Tipo de pagamento',
	'commande_echeances_type_annee' => 'Pagamento anual',
	'commande_echeances_type_mois' => 'Pagamento mensal',
	'commande_echeances_type_vide' => 'Pagamento único',
	'commande_editer' => 'Editar o pedido',
	'commande_modifier' => 'Modificar o pedido:',
	'commande_montant' => 'Montante',
	'commande_nouvelle' => 'Novo pedido',
	'commande_numero' => 'Pedido ',
	'commande_reference' => 'Referência',
	'commande_reference_numero' => 'Referência n° ',
	'commande_statut' => 'Estatuto',
	'commande_titre' => 'Pedido',
	'commandes_titre' => 'Pedidos',
	'configurer_notifications_commandes' => 'Configurar as notificações',
	'configurer_titre' => 'Configurar o plugin "Commandes"',
	'confirmer_supprimer_commande' => 'Quer confirmar o cancelamento do pedido?',
	'confirmer_supprimer_detail' => 'Confirma a remoção deste detalhe do pedido?', # MODIF
	'contact_label' => 'Contacto:',
	'contenu' => 'Conteúdo',

	// D
	'date_commande_label' => 'Data de criação',
	'date_commande_label_court' => 'Criação',
	'date_envoi_label' => 'Data de envio',
	'date_envoi_label_court' => 'Envio',
	'date_maj_label' => 'Última actualização',
	'date_paiement_label' => 'Data de pagamento',
	'date_paiement_label_court' => 'Pagamento',
	'designation' => 'Designação',
	'detail_ajouter' => 'Adicionar um detalhe de pedido', # MODIF
	'detail_champ_descriptif_explication' => 'Pode ser deixado vazio se for especificado um conteúdo SPIP existente. Neste caso, o título do conteúdo será utilizado automaticamente.',
	'detail_champ_descriptif_label' => 'Descritivo',
	'detail_champ_id_objet_label' => 'Identificador de conteúdo SPIP',
	'detail_champ_objet_label' => 'Tipo de conteúdo SPIP',
	'detail_champ_prix_unitaire_ht_label' => 'Preço unitário sem IVA',
	'detail_champ_quantite_label' => 'Quantidade',
	'detail_champ_reduction_label' => 'Redução',
	'detail_champ_taxe_label' => 'Imposto',
	'detail_creer' => 'Criar um detalhe do pedido', # MODIF
	'detail_modifier' => 'Modificar este detalhe do pedido', # MODIF
	'detail_titre' => 'Detalhe do pedido', # MODIF
	'details_commande' => 'Detalhes do pedido:', # MODIF
	'details_titre' => 'Detalhes do pedido', # MODIF

	// E
	'erreur_reference_existante' => 'Já existe um pedido com a mesma referência.',
	'erreur_reference_inexistante' => 'Não há nenhum pedido com esta referência.',
	'etat' => 'Estado',
	'explication_accueil_encours' => 'Exibir os pedidos activos na página inicial?',
	'explication_bank_uid' => 'Identificador único da assinatura no serviço bancário.',
	'explication_choix_statuts_actifs' => 'Estado(s) correspondente(s) aos pedidos activos',
	'explication_statuts_actifs' => 'Dependendo do seu estado, alguns pedidos podem exigir a sua atenção. Estes são pedidos "activos", que requerem acção da sua parte. Pode fazê-los aparecer na lista de artigos que aguardam validação na página inicial.',
	'explications_notifications_statuts' => 'Estado(s) que desencadeia(m) o envio de uma notificação:',
	'explications_notifications_statuts_aucune' => 'As notificações estão desactivadas',

	// I
	'info_1_commande' => '1 pedido',
	'info_1_commande_active' => '1 pedido activo',
	'info_1_commande_statut_abandonne' => '1 pedido abandonado',
	'info_1_commande_statut_attente' => '1 pedido pendente de validação',
	'info_1_commande_statut_encours' => '1 pedido em curso',
	'info_1_commande_statut_envoye' => '1 pedido enviado',
	'info_1_commande_statut_erreur' => '1 pedido errado',
	'info_1_commande_statut_partiel' => '1 pedido parcialmente pago',
	'info_1_commande_statut_paye' => '1 pedido pago',
	'info_1_commande_statut_retour' => '1 pedido devolvido',
	'info_1_commande_statut_retour_partiel' => '1 pedido parcialmente devolvido',
	'info_1_detail' => '1 detalhe de pedido', # MODIF
	'info_aucun_client' => 'Nenhum autor está associado ao pedido',
	'info_aucun_commande' => 'Nenhum pedido',
	'info_aucun_detail' => 'Sem detalhes do pedido', # MODIF
	'info_commande_vide' => 'O pedido não contém quaisquer artigos',
	'info_commandes' => 'Pedidos',
	'info_date_envoi_vide' => 'pedido não enviado',
	'info_date_non_definie' => 'não definido',
	'info_date_paiement_vide' => 'pedido não pago',
	'info_nb_commandes' => '@nb@ pedidos',
	'info_nb_commandes_actives' => '@nb@ pedidos activos',
	'info_nb_commandes_statut_abandonne' => '@nb@ pedidos abandonados',
	'info_nb_commandes_statut_attente' => '@nb@ pedidos pendentes de validação',
	'info_nb_commandes_statut_envoye' => '@nb@ pedidos enviados',
	'info_nb_commandes_statut_erreur' => '@nb@ pedidos errados',
	'info_nb_commandes_statut_partiel' => '@nb@ pedidos parcialmente pagos',
	'info_nb_commandes_statut_paye' => '@nb@ pedidos pagos',
	'info_nb_commandes_statut_retour' => '@nb@ pedidos devolvidos',
	'info_nb_commandes_statut_retour_partiel' => '@nb@ pedidos parcialmente devolvidos',
	'info_nb_commandse_statut_encours' => '@nb@ pedidos em curso',
	'info_nb_details' => '@nb@ detalhes do pedido', # MODIF
	'info_numero' => 'PEDIDO NÚMERO:',
	'info_numero_commande' => 'PEDIDO NÚMERO:',
	'info_sans_descriptif' => 'Sem descrição',
	'info_toutes_commandes' => 'Todos os pedidos',

	// L
	'label_actions' => 'Acções',
	'label_bank_uid' => 'UID da assinatura bancária',
	'label_commande_dates' => 'Datas',
	'label_dont_taxe' => 'dos quais imposto',
	'label_filtre_clients' => 'Clientes',
	'label_filtre_dates' => 'Datas',
	'label_filtre_echeances_type' => 'Tipo de pagamento',
	'label_filtre_etats' => 'Estados',
	'label_filtre_paiement' => 'Modo de pagamento',
	'label_filtre_tous' => 'Todos',
	'label_filtre_tous_clients' => 'Todos os clientes',
	'label_filtre_tous_echeances_type' => 'Todos os tipos de pagamento',
	'label_filtre_tous_mode_paiements' => 'Todos os modos',
	'label_filtre_tous_statuts' => 'Todos os estados',
	'label_filtre_toutes' => 'Todas',
	'label_filtre_toutes_dates' => 'Todas as datas',
	'label_infos' => 'Informações',
	'label_montant_ttc' => 'Montante com IVA',
	'label_objet' => 'Conteúdo associado ',
	'label_objets' => 'Conteúdos ligados',
	'label_passee_le' => 'efectuada a ',
	'label_payee_le' => 'pago a ',
	'label_prix' => 'Preço',
	'label_prix_unitaire' => 'Preço unitário sem IVA',
	'label_quantite' => 'Quantidade',
	'label_recherche' => 'Pesquisar',
	'label_reduction' => 'Redução.',
	'label_statuts_actifs' => 'Estatutos',
	'label_taxe' => 'Imposto',
	'label_total_ht' => 'Total sem impostos',

	// M
	'merci_de_votre_commande' => 'Registámos correctamente o seu pedido e agradecemos-lhe a sua confiança.',
	'merci_de_votre_commande_paiement' => 'Registámos o seu pedido <b>@reference@</b>, e iremos processá-lo o mais rapidamente possível.',
	'modifier_commande_statut' => 'Este pedido é:',
	'montant' => 'Montante',

	// N
	'nom_bouton_plugin' => 'Pedidos',
	'notifications_activer_explication' => 'Enviar notificações de encomenda por e-mail?',
	'notifications_activer_label' => 'Activar',
	'notifications_cfg_titre' => 'Notificações',
	'notifications_client_explication' => 'Enviar as notificações ao cliente?',
	'notifications_client_label' => 'Cliente',
	'notifications_expediteur_administrateur_label' => 'Escolher um administrador:',
	'notifications_expediteur_choix_administrateur' => 'um administrador',
	'notifications_expediteur_choix_email' => 'um e-mail',
	'notifications_expediteur_choix_facteur' => 'idem plugin "Facteur"',
	'notifications_expediteur_choix_webmaster' => 'um webmaster',
	'notifications_expediteur_email_label' => 'Email do remetente:',
	'notifications_expediteur_explication' => 'Escolher o remetente das notificações para o vendedor e para o comprador',
	'notifications_expediteur_label' => 'Remetente',
	'notifications_expediteur_webmaster_label' => 'Escolher um webmaster:',
	'notifications_explication' => 'As notificações permitem-lhe enviar e-mails após alterações do estado da encomenda: Validação Pendente, Em Curso, Enviado, Parcialmente Pago, Pago, Devolvido, Devolvido Parcialmente. Esta funcionalidade requer <a href="https://plugins.spip.net/notifavancees.html">o plugin Notificações Avançadas</a>.',
	'notifications_parametres' => 'Configurações das notificações',
	'notifications_quand_explication' => 'Que alteração(ões) no estatuto desencadeiam o envio de uma notificação?',
	'notifications_quand_label' => 'Activação',
	'notifications_vendeur_administrateur_label' => 'Escolher um ou mais administradores:',
	'notifications_vendeur_choix_administrateur' => 'um ou mais administradores',
	'notifications_vendeur_choix_email' => 'um ou mais emails',
	'notifications_vendeur_choix_webmaster' => 'um ou mais webmasters',
	'notifications_vendeur_email_explication' => 'Introduza um ou mais emails separados por vírgulas:',
	'notifications_vendeur_email_label' => 'Email(s) do vendedor:',
	'notifications_vendeur_explication' => 'Escolher o(s) destinatário(s) das notificações para envio ao vendedor',
	'notifications_vendeur_label' => 'Vendedor',
	'notifications_vendeur_webmaster_label' => 'Escolher um ou mais webmasters:',

	// P
	'parametres_cfg_titre' => 'Parâmetros',
	'parametres_duree_vie_explication' => 'Limitar a vida útil (em horas) de um pedido com o estatuto em curso, antes de ser considerada abandonada.',
	'parametres_duree_vie_label' => 'Vida útil',
	'passer_la_commande' => 'Colocar o pedido',

	// R
	'recapitulatif' => 'Resumo do pedido:',
	'reference' => 'Referência',
	'reference_label' => 'Referência: ',
	'reference_ref' => 'Referência @ref@',

	// S
	'simuler' => 'Simular mudança de estatuto',
	'statut_abandonne' => 'Abandonado',
	'statut_attente' => 'Pendente de validação',
	'statut_encours' => 'Em curso',
	'statut_envoye' => 'Enviado',
	'statut_erreur' => 'Erro',
	'statut_label' => 'Estatuto:',
	'statut_partiel' => 'Parcialmente pago',
	'statut_paye' => 'pago',
	'statut_poubelle' => 'Lixo',
	'statut_retour' => 'Devolvida',
	'statut_retour_partiel' => 'Devolução parcial',
	'supprimer' => 'Remover',

	// T
	'texte_changer_statut_commande' => 'Este pedido é:',
	'texte_changer_statut_commande_detail' => 'Este detalhe do pedido é :', # MODIF
	'titre_adresse_client' => 'Endereço do cliente',
	'titre_adresse_commande' => 'Endereço associado ao pedido',
	'titre_adresse_contact' => 'Endereço do contacto',
	'titre_adresses_associees' => 'Endereços associados',
	'titre_adresses_client' => 'Endereços do cliente',
	'titre_adresses_commande' => 'Endereços associados ao pedido',
	'titre_commandes_actives' => 'Pedidos activos',
	'titre_commandes_auteur' => 'Pedidos do autor',
	'titre_contenu_commande' => 'Conteúdo do pedido',
	'titre_informations_client' => 'Cliente',
	'titre_logo_commande' => 'Logotipo do pedido',
	'titre_statuts_actifs_parametres' => 'Pedidos activos',
	'type_adresse_facturation' => 'Facturação',
	'type_adresse_livraison' => 'Entrega',

	// U
	'une_commande_de' => 'Um pedido de: ',
	'une_commande_sur' => 'Um pedido de @nom@',

	// V
	'votre_commande_sur' => '@nom@ : o seu pedido'
);
