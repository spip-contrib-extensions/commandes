<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/commandes.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abbr_hors_taxe' => 'HT',
	'abbr_prix_unitaire' => 'P.U',
	'abbr_quantite' => 'Qté',
	'abbr_total_ht' => 'Total HT',
	'abbr_toutes_taxes_comprises' => 'TTC',
	'action_modifier' => 'Modifier',
	'action_supprimer' => 'Supprimer',
	'action_supprimer_detail' => 'Supprimer cette ligne',

	// B
	'bonjour' => 'Bonjour',

	// C
	'commande_client' => 'Client',
	'commande_creer' => 'Créer une commande',
	'commande_date' => 'Date',
	'commande_date_paiement' => 'Date de paiement',
	'commande_echeances_date_debut' => 'Date de début des paiements périodiques :',
	'commande_echeances_paiements_infini' => 'Paiements suivants',
	'commande_echeances_paiements_previsions' => 'Paiements prévisionnels',
	'commande_echeances_paiements_tous' => 'Tous les paiements',
	'commande_echeances_type' => 'Type de paiement',
	'commande_echeances_type_annee' => 'Paiement annuel',
	'commande_echeances_type_mois' => 'Paiement mensuel',
	'commande_echeances_type_vide' => 'Paiement unique',
	'commande_editer' => 'Éditer la commande',
	'commande_modifier' => 'Modifier la commande :',
	'commande_montant' => 'Montant',
	'commande_nouvelle' => 'Nouvelle commande',
	'commande_numero' => 'Commande ',
	'commande_reference' => 'Référence',
	'commande_reference_numero' => 'Référence n° ', # MODIF
	'commande_statut' => 'Statut',
	'commande_titre' => 'Commande',
	'commandes_titre' => 'Commandes',
	'configurer_notifications_commandes' => 'Configurer les notifications',
	'configurer_titre' => 'Configurer le plugin Commandes',
	'confirmer_supprimer_commande' => 'Confirmez-vous la suppression de la commande ?',
	'confirmer_supprimer_detail' => 'Confirmez-vous la suppression de cette ligne de la commande ?',
	'contact_label' => 'Contact :',
	'contenu' => 'Contenu',

	// D
	'date_commande_label' => 'Date de création',
	'date_commande_label_court' => 'Création',
	'date_envoi_label' => 'Date d’envoi',
	'date_envoi_label_court' => 'Envoi',
	'date_maj_label' => 'Dernière mise à jour',
	'date_paiement_label' => 'Date de paiement',
	'date_paiement_label_court' => 'Paiement',
	'designation' => 'Désignation',
	'detail_ajouter' => 'Ajouter une ligne à la commande',
	'detail_champ_descriptif_explication' => 'Peut être laissé vide si l’on indique un contenu SPIP existant. Dans ce cas le titre du contenu sera utilisé automatiquement.',
	'detail_champ_descriptif_label' => 'Descriptif',
	'detail_champ_id_objet_label' => 'Identifiant du contenu SPIP',
	'detail_champ_objet_label' => 'Type du contenu SPIP',
	'detail_champ_prix_unitaire_ht_label' => 'Prix unitaire HT',
	'detail_champ_quantite_label' => 'Quantité',
	'detail_champ_reduction_label' => 'Réduction',
	'detail_champ_taxe_label' => 'Taxe',
	'detail_creer' => 'Créer une ligne dans la commande',
	'detail_modifier' => 'Modifier cette ligne',
	'detail_titre' => 'Ligne de la commande',
	'details_commande' => 'Lignes de la commande :',
	'details_titre' => 'Lignes de commande',

	// E
	'erreur_reference_existante' => 'Une commande avec la même référence existe déjà.',
	'erreur_reference_inexistante' => 'Il n’existe pas de commande avec cette référence.',
	'etat' => 'État',
	'explication_accueil_encours' => 'Signaler les commandes actives sur la page d’accueil ?',
	'explication_bank_uid' => 'Identifiant unique de l’abonnement chez le prestataire bancaire.',
	'explication_choix_statuts_actifs' => 'Statut(s) correspondant(s) aux commandes actives',
	'explication_statuts_actifs' => 'Selon leur statut, certaines commandes peuvent requérir votre attention. Il s’agit des commandes « actives », nécessitant une action de votre part. Vous pouvez les faire apparaître dans la liste des éléments en attente de validation sur la page d’accueil.',
	'explications_notifications_statuts' => 'État(s) déclenchant l’envoi d’une notification :',
	'explications_notifications_statuts_aucune' => 'Les notifications sont désactivées',

	// I
	'info_1_commande' => '1 commande',
	'info_1_commande_active' => '1 commande active',
	'info_1_commande_statut_abandonne' => '1 commande abondonnée',
	'info_1_commande_statut_attente' => '1 commande en attente de validation',
	'info_1_commande_statut_encours' => '1 commande en cours',
	'info_1_commande_statut_envoye' => '1 commande envoyée',
	'info_1_commande_statut_erreur' => '1 commande erronée',
	'info_1_commande_statut_partiel' => '1 commande partiellement payée',
	'info_1_commande_statut_paye' => '1 commande payée',
	'info_1_commande_statut_retour' => '1 commande retournée',
	'info_1_commande_statut_retour_partiel' => '1 commande partiellement retournée',
	'info_1_detail' => '1 ligne',
	'info_aucun_client' => 'Aucun auteur n’est associé à la commande',
	'info_aucun_commande' => 'Aucune commande',
	'info_aucun_detail' => 'Aucune ligne dans la commande',
	'info_commande_vide' => 'La commande ne comporte aucun article',
	'info_commandes' => 'Commandes',
	'info_date_envoi_vide' => 'commande non envoyée',
	'info_date_non_definie' => 'non définie',
	'info_date_paiement_vide' => 'commande non payée',
	'info_nb_commandes' => '@nb@ commandes',
	'info_nb_commandes_actives' => '@nb@ commandes actives',
	'info_nb_commandes_statut_abandonne' => '@nb@ commandes abandonnées',
	'info_nb_commandes_statut_attente' => '@nb@ commandes en attente de validation',
	'info_nb_commandes_statut_envoye' => '@nb@ commandes envoyées',
	'info_nb_commandes_statut_erreur' => '@nb@ commandes erronées',
	'info_nb_commandes_statut_partiel' => '@nb@ commandes partiellement payées',
	'info_nb_commandes_statut_paye' => '@nb@ commandes payées',
	'info_nb_commandes_statut_retour' => '@nb@ commandes retournées',
	'info_nb_commandes_statut_retour_partiel' => '@nb@ commandes partiellement retournées',
	'info_nb_commandse_statut_encours' => '@nb@ commandes en cours',
	'info_nb_details' => '@nb@ lignes',
	'info_numero' => 'COMMANDE NUMÉRO :',
	'info_numero_commande' => 'COMMANDE NUMÉRO :',
	'info_sans_descriptif' => 'Sans descriptif',
	'info_toutes_commandes' => 'Toutes les commandes',

	// L
	'label_actions' => 'Actions',
	'label_bank_uid' => 'UID de l’abonnement bancaire',
	'label_commande_dates' => 'Dates',
	'label_dont_taxe' => 'dont taxe',
	'label_filtre_clients' => 'Clients',
	'label_filtre_dates' => 'Dates',
	'label_filtre_echeances_type' => 'Type de paiement',
	'label_filtre_etats' => 'Etats',
	'label_filtre_paiement' => 'Mode de paiement',
	'label_filtre_tous' => 'Tous',
	'label_filtre_tous_clients' => 'Tous les clients',
	'label_filtre_tous_echeances_type' => 'Tous types de paiement',
	'label_filtre_tous_mode_paiements' => 'Tous les modes',
	'label_filtre_tous_statuts' => 'Tous les états',
	'label_filtre_toutes' => 'Toutes',
	'label_filtre_toutes_dates' => 'Toutes les dates',
	'label_infos' => 'Infos',
	'label_montant_ttc' => 'Montant TTC',
	'label_objet' => 'Contenu lié',
	'label_objets' => 'Contenus liés',
	'label_passee_le' => 'passée le',
	'label_payee_le' => 'payée le',
	'label_prix' => 'Prix',
	'label_prix_unitaire' => 'Prix unitaire HT',
	'label_quantite' => 'Quantité',
	'label_recherche' => 'Rechercher',
	'label_reduction' => 'Reduc.',
	'label_statuts_actifs' => 'Statuts',
	'label_taxe' => 'Taxe',
	'label_total_ht' => 'Total Hors Taxe',

	// M
	'merci_de_votre_commande' => 'Nous avons bien enregistré votre commande et nous vous remercions de votre confiance.',
	'merci_de_votre_commande_paiement' => 'Nous avons bien enregistré votre commande <b>@reference@</b>, et nous la traitons dans les meilleurs délais.',
	'modifier_commande_statut' => 'Cette commande est :',
	'montant' => 'Montant',

	// N
	'nom_bouton_plugin' => 'Commandes',
	'notifications_activer_explication' => 'Envoyer par mail des notifications de commande ?',
	'notifications_activer_label' => 'Activer',
	'notifications_cfg_titre' => 'Notifications',
	'notifications_client_explication' => 'Envoyer les notifications au client ?',
	'notifications_client_label' => 'Client',
	'notifications_expediteur_administrateur_label' => 'Choisir un administrateur :',
	'notifications_expediteur_choix_administrateur' => 'un administrateur',
	'notifications_expediteur_choix_email' => 'un email',
	'notifications_expediteur_choix_facteur' => 'idem plugin Facteur',
	'notifications_expediteur_choix_webmaster' => 'un webmestre',
	'notifications_expediteur_email_label' => 'Email de l’expéditeur :',
	'notifications_expediteur_explication' => 'Choisir l’expéditeur des notifications pour le vendeur et l’acheteur',
	'notifications_expediteur_label' => 'Expéditeur',
	'notifications_expediteur_webmaster_label' => 'Choisir un webmestre :',
	'notifications_explication' => 'Les notifications permettent d’envoyer des emails suite aux changements de statut des commandes : En attente de validation, en cours, envoyée, partiellement payée, payée, retournée, retour partiel. Cette fonctionnalité nécessite <a href="https://plugins.spip.net/notifavancees.html">le plugin Notifications Avancées</a>.',
	'notifications_parametres' => 'Paramètres des notifications',
	'notifications_quand_explication' => 'Quel(s) changement(s) de statut déclenche(nt) l’envoi d’une notification ?',
	'notifications_quand_label' => 'Déclenchement',
	'notifications_vendeur_administrateur_label' => 'Choisir un ou plusieurs administrateurs :',
	'notifications_vendeur_choix_administrateur' => 'un ou des administrateurs',
	'notifications_vendeur_choix_email' => 'un ou des emails',
	'notifications_vendeur_choix_webmaster' => 'un ou des webmestres',
	'notifications_vendeur_email_explication' => 'Saisir un ou plusieurs email séparés par des virgules :',
	'notifications_vendeur_email_label' => 'Email(s) du vendeur :',
	'notifications_vendeur_explication' => 'Choisir le(s) destinataire(s) des notifications pour les envois au vendeur',
	'notifications_vendeur_label' => 'Vendeur',
	'notifications_vendeur_webmaster_label' => 'Choisir un ou plusieurs webmestres :',

	// P
	'parametres_cfg_titre' => 'Paramètres',
	'parametres_duree_vie_explication' => 'Limiter la durée de vie (en heures) d’une commande avec le statut en cours, avant qu’elle ne soit considérée comme étant abandonnée.',
	'parametres_duree_vie_label' => 'Durée de vie',
	'passer_la_commande' => 'Passer la commande',

	// R
	'recapitulatif' => 'Récapitulatif de commande :',
	'reference' => 'Référence',
	'reference_label' => 'Référence :',
	'reference_ref' => 'Référence @ref@',

	// S
	'simuler' => 'Simuler changement de statut',
	'statut_abandonne' => 'Abandonnée',
	'statut_attente' => 'En attente de validation',
	'statut_attente_echeance' => 'En attente de l’échéance',
	'statut_encours' => 'En cours',
	'statut_envoye' => 'Envoyée',
	'statut_erreur' => 'Erreur',
	'statut_label' => 'Statut :',
	'statut_partiel' => 'Partiellement payée',
	'statut_paye' => 'Payée',
	'statut_poubelle' => 'Poubelle',
	'statut_retour' => 'Retournée',
	'statut_retour_partiel' => 'Retour partiel',
	'supprimer' => 'Supprimer',

	// T
	'texte_changer_statut_commande' => 'Cette commande est :',
	'texte_changer_statut_commande_detail' => 'Cette ligne de commande est :',
	'titre_adresse_client' => 'Adresse du client',
	'titre_adresse_commande' => 'Adresse associée à la commande',
	'titre_adresse_contact' => 'Adresse du contact',
	'titre_adresses_associees' => 'Adresses associées',
	'titre_adresses_client' => 'Adresses du client',
	'titre_adresses_commande' => 'Adresses associées à la commande',
	'titre_commandes_actives' => 'Commandes actives',
	'titre_commandes_auteur' => 'Commandes de l’auteur',
	'titre_contenu_commande' => 'Contenu de la commande',
	'titre_informations_client' => 'Client',
	'titre_logo_commande' => 'Logo de la commande',
	'titre_statuts_actifs_parametres' => 'Commandes actives',
	'type_adresse_facturation' => 'Facturation',
	'type_adresse_livraison' => 'Livraison',

	// U
	'une_commande_de' => 'Une commande de : ',
	'une_commande_sur' => 'Une commande sur @nom@',

	// V
	'votre_commande_sur' => '@nom@ : votre commande'
);
