<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// S
	'commandes_description' => 'Permet de gérer des commandes.',
	'commandes_nom' => 'Commandes',
	'commandes_slogan' => 'Gérer des commandes',
);

?>
