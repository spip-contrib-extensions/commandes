<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/commandes?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'abbr_hors_taxe' => 'DPH',
	'abbr_prix_unitaire' => 'JC',
	'abbr_quantite' => 'Množ.',
	'abbr_total_ht' => 'Celkom bez DPH',
	'abbr_toutes_taxes_comprises' => 's DPH',
	'action_modifier' => 'Zmeniť',
	'action_supprimer' => 'Odstrániť',

	// B
	'bonjour' => 'Dobrý deň',

	// C
	'commande_client' => 'Zákazník',
	'commande_date' => 'Dátum',
	'commande_date_paiement' => 'Dátum platby',
	'commande_echeances_type_mois' => 'Mesačná platba',
	'commande_editer' => 'Upraviť objednávku',
	'commande_modifier' => 'Zmeniť objednávku:',
	'commande_montant' => 'Množstvo',
	'commande_nouvelle' => 'Nová objednávka',
	'commande_numero' => 'Objednávka',
	'commande_reference' => 'Číslo',
	'commande_reference_numero' => 'Č. objednávky ',
	'commande_statut' => 'Štádium',
	'commande_titre' => 'Objednávka',
	'commandes_titre' => 'Objednávky',
	'configurer_notifications_commandes' => 'Nastaviť oznamy',
	'configurer_titre' => 'Nastaviť zásuvný modul Objednávky',
	'confirmer_supprimer_commande' => 'Môžete potvrdiť odstránenie tejto objednávky?',
	'contact_label' => 'Kontakt:',
	'contenu' => 'Obsah',

	// D
	'date_commande_label' => 'Dátum vytvorenia',
	'date_commande_label_court' => 'Vytvorenie',
	'date_envoi_label' => 'Dátum odoslania',
	'date_envoi_label_court' => 'Odoslanie',
	'date_maj_label' => 'Naposledy aktualizované',
	'date_paiement_label' => 'Dátum platby',
	'date_paiement_label_court' => 'Platba',
	'designation' => 'Označenie',
	'detail_ajouter' => 'Pridať údaj objednávky', # MODIF
	'detail_champ_descriptif_label' => 'Opis',
	'detail_champ_prix_unitaire_ht_label' => 'Jednotková cena bez DPH',
	'detail_champ_quantite_label' => 'Množstvo',
	'detail_champ_taxe_label' => 'Daň',
	'detail_titre' => 'Údaj objednávky', # MODIF
	'details_commande' => 'Podrobnosti objednávky:', # MODIF
	'details_titre' => 'Podrobnosti objednávky', # MODIF

	// E
	'etat' => 'Stav',
	'explications_notifications_statuts' => 'Posielanie oznamov:',
	'explications_notifications_statuts_aucune' => 'Oznamy sú deaktivované',

	// I
	'info_1_commande' => '1 objednávka',
	'info_1_commande_statut_attente' => '1 objednávka čaká na potvrdenie',
	'info_1_commande_statut_envoye' => '1 odoslaná objednávka',
	'info_1_commande_statut_partiel' => '1 objednávka bola čiastočne zaplatená',
	'info_1_commande_statut_paye' => '1 objednávka bola zaplatená',
	'info_1_commande_statut_retour' => '1 vrátená objednávka',
	'info_1_commande_statut_retour_partiel' => '1 objednávka vrátená čiastočne ',
	'info_1_detail' => '1 údaj objednávky', # MODIF
	'info_aucun_commande' => 'Žiadna objednávka',
	'info_commandes' => 'Objednávky',
	'info_date_envoi_vide' => 'neodoslaná objednávka',
	'info_date_non_definie' => 'nezadané',
	'info_date_paiement_vide' => 'nezaplatená objednávka',
	'info_nb_commandes' => '@nb@ objednávok',
	'info_nb_commandes_statut_attente' => '@nb@ objednávok čaká na potvrdenie',
	'info_nb_commandes_statut_envoye' => '@nb@ odoslaných objednávok',
	'info_nb_commandes_statut_partiel' => '@nb@ objednávok bolo čiastočne zaplatených',
	'info_nb_commandes_statut_paye' => '@nb@ objednávok bolo zaplatených',
	'info_nb_commandes_statut_retour' => '@nb@ vrátených objednávok',
	'info_nb_commandes_statut_retour_partiel' => '@nb@ objednávok vrátených čiastočne',
	'info_numero' => 'OBJEDNÁVKA ČÍSLO:',
	'info_numero_commande' => 'OBJEDNÁVKA ČÍSLO:',
	'info_sans_descriptif' => 'Bez opisu',
	'info_toutes_commandes' => 'Všetky objednávky',

	// L
	'label_commande_dates' => 'Dátumy',
	'label_dont_taxe' => 's daňou',
	'label_filtre_clients' => 'Zákazníci',
	'label_filtre_dates' => 'Dátumy',
	'label_filtre_etats' => 'Stavy',
	'label_filtre_tous' => 'Všetko',
	'label_filtre_tous_clients' => 'Všetci zákazníci',
	'label_filtre_tous_statuts' => 'Všetky stavy',
	'label_filtre_toutes_dates' => 'Všetky dátumy',
	'label_infos' => 'Informácie',
	'label_montant_ttc' => 'Cena s DPH',
	'label_objet' => 'Súvisiaci obsah',
	'label_objets' => 'Podobné objekty',
	'label_passee_le' => 'objednané',
	'label_payee_le' => 'zaplatené',
	'label_prix' => 'Cena',
	'label_prix_unitaire' => 'Jednotková cena bez DPH',
	'label_quantite' => 'Množstvo',
	'label_recherche' => 'Vyhľadávať',
	'label_statuts_actifs' => 'Štádiá spracovania',
	'label_taxe' => 'Daň',
	'label_total_ht' => 'Celkom bez DPH',

	// M
	'merci_de_votre_commande' => 'Vašu objednávku sme zaregistrovali a ceníme si Vašu dôveru.',
	'modifier_commande_statut' => 'Táto objednávka je:',
	'montant' => 'Množstvo',

	// N
	'nom_bouton_plugin' => 'Objednávky',
	'notifications_activer_explication' => 'Posielať e-mailom oznamy o príkaze?',
	'notifications_activer_label' => 'Aktivovať',
	'notifications_cfg_titre' => 'Oznamy',
	'notifications_client_explication' => 'Posielať zákazníkovi oznamy?',
	'notifications_client_label' => 'Zákazník',
	'notifications_expediteur_administrateur_label' => 'Vybrať administrátora:',
	'notifications_expediteur_choix_administrateur' => 'administrátor',
	'notifications_expediteur_choix_email' => 'e-mail',
	'notifications_expediteur_choix_facteur' => 'rovnaký ako zásuvný modul Facteur',
	'notifications_expediteur_choix_webmaster' => 'webmaster',
	'notifications_expediteur_email_label' => 'E-mail odosielateľa:',
	'notifications_expediteur_explication' => 'Vyberte odosielateľa oznamov pre predávajúceho a kupujúceho',
	'notifications_expediteur_label' => 'Odosielateľ',
	'notifications_expediteur_webmaster_label' => 'Vybrať webmastera:',
	'notifications_explication' => 'Oznamy sa používajú na posielanie e-mailov po zmenách v stave spracovania objednávky: čaká, vybavuje sa, odoslaná, čiastočne zaplatená, zaplatená, vrátená, čiastočne vrátiť. Táto funkcia si vyžaduje zásuvný modul Podrobné oznamy.', # MODIF
	'notifications_parametres' => 'Parametre oznamov',
	'notifications_quand_explication' => 'Pri akých zmenách stavu poslať oznam?',
	'notifications_quand_label' => 'Spustenie',
	'notifications_vendeur_administrateur_label' => 'Vyberte jedného administrátora alebo viacerých:',
	'notifications_vendeur_choix_administrateur' => 'jeden administrátor alebo viacerí',
	'notifications_vendeur_choix_email' => 'jeden e-mail alebo viacero e-mailov',
	'notifications_vendeur_choix_webmaster' => 'jeden webmaster alebo viacerí',
	'notifications_vendeur_email_explication' => 'Zadajte jednu e-mailovú adresu alebo viac e-mailových adries a oddeľte ich čiarkami:',
	'notifications_vendeur_email_label' => 'E-mail(y) predávajúceho:',
	'notifications_vendeur_explication' => 'Vyberte príjemcu (-ov) oznamov na odoslanie predávajúcemu ',
	'notifications_vendeur_label' => 'Predávajúci',
	'notifications_vendeur_webmaster_label' => 'Vyberte jedného webmastera alebo viacerých:',

	// P
	'parametres_cfg_titre' => 'Parametre',
	'parametres_duree_vie_explication' => 'Zadajte čas platnosti objednávky v tomto štádiu spracovania (v hodinách):', # MODIF
	'parametres_duree_vie_label' => 'Čas platnosti',
	'passer_la_commande' => 'Zadať objednávku',

	// R
	'recapitulatif' => 'Zhrnutie objednávky:',
	'reference' => 'Značka',
	'reference_label' => 'Značka:',
	'reference_ref' => 'Značka @ref@',

	// S
	'simuler' => 'Simulovať zmenu stavu',
	'statut_attente' => 'Čaká',
	'statut_encours' => 'Vybavuje sa',
	'statut_envoye' => 'Odoslaná',
	'statut_erreur' => 'Chyba',
	'statut_label' => 'Stav:',
	'statut_partiel' => 'Čiastočne zaplatená',
	'statut_paye' => 'Zaplatená',
	'statut_poubelle' => 'Kôš',
	'statut_retour' => 'Vrátená',
	'statut_retour_partiel' => 'Čiastočne vrátiť',
	'supprimer' => 'Odstrániť',

	// T
	'texte_changer_statut_commande' => 'Táto objednávka je:',
	'titre_adresse_client' => 'Adresa odberateľa',
	'titre_adresse_commande' => 'Adresa priradená k objednávke',
	'titre_adresse_contact' => 'Adresa',
	'titre_adresses_associees' => 'Priradené adresy',
	'titre_adresses_client' => 'Adresa odberateľa',
	'titre_adresses_commande' => 'Adresy priradené k objednávke',
	'titre_commandes_actives' => 'Aktívne objednávky',
	'titre_commandes_auteur' => 'Objednávky od zákazníka',
	'titre_contenu_commande' => 'Obsah objednávky',
	'titre_informations_client' => 'Odberateľ',
	'titre_logo_commande' => 'Logo objednávky',
	'type_adresse_facturation' => 'Fakturácia',
	'type_adresse_livraison' => 'Dodanie',

	// U
	'une_commande_de' => 'Objednávka: ',
	'une_commande_sur' => 'Vaša objednávka – @nom@',

	// V
	'votre_commande_sur' => '@nom@: Vaša objednávka'
);
