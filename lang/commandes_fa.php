<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/commandes?lang_cible=fa
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'action_supprimer' => 'حذف ',

	// C
	'commande_client' => 'خريدار',
	'commande_date' => 'تاريخ سفارش', # MODIF
	'commande_date_paiement' => 'تاريخ پرداخت', # MODIF
	'commande_montant' => 'مبلغ',
	'commande_reference' => 'مرجع',
	'commande_statut' => 'وضعيت',
	'confirmer_supprimer_commande' => 'حذف اين سفارش را تأييد مي‌كنيد؟',
	'contenu' => 'مفاد',

	// D
	'designation' => 'عنوان',

	// E
	'etat' => 'وضعيت',

	// I
	'info_toutes_commandes' => 'تمام سفارش‌ها',

	// M
	'montant' => 'مبلغ',

	// N
	'nom_bouton_plugin' => 'سفارش‌ها',

	// R
	'reference' => 'ارجاع',

	// S
	'statut_attente' => 'در دست بررسي',
	'statut_encours' => 'در جريان',
	'statut_envoye' => 'ارسال شده',
	'statut_partiel' => 'بخشي از مبلغ پرداخت شده',
	'statut_paye' => 'پرداخت',
	'statut_retour' => 'برگشت',
	'statut_retour_partiel' => 'مبلغ برگشتي'
);
