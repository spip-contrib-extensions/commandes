<?php

/**
 * Fonction du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Notifications
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Détermine le ou les vendeurs destinataires des notifications d'une commande
 *
 * @param int $id_commande
 *     Identifiant de la commande
 * @param array $options
 *     options
 * @return array
 *     Liste des destinataires
 */
function notifications_commande_vendeur_destinataires_dist($id_commande, $options) {

	include_spip('inc/config');
	$config = lire_config('commandes');
	$destinataire = $config['vendeur_' . $config['vendeur']];
	return is_array($destinataire) ? $destinataire : [$destinataire];
}


/**
 * Calcul le contenu des notifications si une variante selon le statut/statut_ancien est disponible
 *
 * Cherche dans l'ordre un squelette
 * commande_vendeur_{$statut}_depuis_{$statut_ancien}
 * commande_vendeur_{$statut}
 * commande_vendeur_depuis_{$statut_ancien}
 * commande_vendeur
 * pour chaque partie ['', '_court', '_html']
 *
 * Si on en trouve un, on renseigne la valeur, sinon on laisse vide et notifications_envoyer() finira le traitement
 *
 * @uses commande_notification_contenu_dist()
 * @see notifications_envoyer()
 *
 * @param int $id
 * @param array $options
 * @param array $destinataire
 * @param string $mode
 * @return array
 */
function notifications_commande_vendeur_contenu_dist($id, $options, $destinataire, $mode, $contexte = null) {
	$notification_contenu = charger_fonction('notification_contenu', 'commande');
	return $notification_contenu('commande_vendeur', $id, $options, $destinataire, $mode, $contexte);
}
