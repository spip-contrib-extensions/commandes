<?php

/**
 * Fonction du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Notifications
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Détermine le client destinataire des notifications d'une commande
 *
 * @param int $id_commande
 *     Identifiant de la commande
 * @param array $options
 *     options
 * @return array
 *     Liste des destinataires
 */
function notifications_commande_client_destinataires_dist($id_commande, $options) {

	$id_auteur = sql_getfetsel('id_auteur', 'spip_commandes', 'id_commande=' . $id_commande);
	return [$id_auteur];
}

/**
 * Calcul le contenu des notifications si une variante selon le statut/statut_ancien est disponible
 *
 * Cherche dans l'ordre un squelette
 * commande_client_{$statut}_depuis_{$statut_ancien}
 * commande_client_{$statut}
 * commande_client_depuis_{$statut_ancien}
 * commande_client
 * pour chaque partie ['', '_court', '_html']
 *
 * Si on en trouve un, on renseigne la valeur, sinon on laisse vide et notifications_envoyer() finira le traitement
 *
 * @uses commande_notification_contenu_dist()
 * @see notifications_envoyer()
 *
 * @param int $id
 * @param array $options
 * @param array $destinataire
 * @param string $mode
 * @return array
 */
function notifications_commande_client_contenu_dist($id, $options, $destinataire, $mode, $contexte = null) {
	$notification_contenu = charger_fonction('notification_contenu', 'commande');
	return $notification_contenu('commande_client', $id, $options, $destinataire, $mode, $contexte);
}
