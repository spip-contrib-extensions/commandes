<?php

/**
 * Gestion du formulaire de configuration du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Formulaires
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Description des saisies du formulaire de configuration
 *
 * @return array
 *     Description des saisies
 */
function formulaires_configurer_commandes_saisies_dist() {

	include_spip('inc/config');
	include_spip('inc/plugin');
	include_spip('commandes_fonctions');
	include_spip('inc/puce_statut');
	$config = lire_config('commandes');
	$saisies = [];
	$choix_expediteurs = [
		'webmaster' => _T('commandes:notifications_expediteur_choix_webmaster'),
		'administrateur' => _T('commandes:notifications_expediteur_choix_administrateur'),
		'email' => _T('commandes:notifications_expediteur_choix_email')
	];
	if (defined('_DIR_PLUGIN_FACTEUR')) {
		$choix_expediteurs['facteur'] = _T('commandes:notifications_expediteur_choix_facteur');
	}

	// liste des statuts précédés de leur puce
	foreach (commandes_lister_statuts() as $k => $v) {
		$statuts[$k] = http_img_pack(statut_image('commande', $k), '') . '&nbsp;' . $v;
	}

	$saisies = [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_parametres',
				'label' => _T('commandes:parametres_cfg_titre')
			],
			'saisies' => [
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'duree_vie',
						'label' => _T('commandes:parametres_duree_vie_label'),
						'explication' => _T('commandes:parametres_duree_vie_explication'),
						'defaut' => ($config['duree_vie'] ? $config['duree_vie'] : 24),
						'type' => 'number',
						'min' => 1,
					],
					'verifier' => [
						'type' => 'entier',
						'options' => ['min' => 1],
					],
				]
			]
		],
	];
	$notifications = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_notifications',
			'label' => _T('commandes:notifications_cfg_titre')
		],
		'saisies' => [
			[
				'saisie' => 'explication',
				'options' => [
					'nom' => 'exp1',
					'texte' => _T('commandes:notifications_explication')
				]
			],
		],
	];
	if (defined('_DIR_PLUGIN_NOTIFAVANCEES')) {
		$notifications['saisies'][] = [
			'saisie' => 'oui_non',
			'options' => [
				'nom' => 'activer',
				'label' => _T('commandes:notifications_activer_label'),
				'explication' => _T('commandes:notifications_activer_explication'),
				'defaut' => $config['activer']
			]
		];
		$saisies[] = $notifications;
		$saisies[] = [
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'fieldset_notifications_parametres',
				'label' => _T('commandes:notifications_parametres'),
				'afficher_si' => '@activer@ == "on"',
			],
			'saisies' => [
				[
					'saisie' => 'checkbox',
					'options' => [
						'nom' => 'quand',
						'label' => _T('commandes:notifications_quand_label'),
						'explication' => _T('commandes:notifications_quand_explication'),
						'data' => $statuts,
						'defaut' => $config['quand']
					]
				],

				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'expediteur',
						'label' => _T('commandes:notifications_expediteur_label'),
						'explication' => _T('commandes:notifications_expediteur_explication'),
						'cacher_option_intro' => 'on',
						'defaut' => $config['expediteur'],
						'data' => $choix_expediteurs
					]
				],

				[
					'saisie' => 'auteurs',
					'options' => [
						'nom' => 'expediteur_webmaster',
						'label' => _T('commandes:notifications_expediteur_webmaster_label'),
						'statut' => '0minirezo',
						'cacher_option_intro' => 'on',
						'webmestre' => 'oui',
						'defaut' => $config['expediteur_webmaster'],
						'afficher_si' => '@expediteur@ == "webmaster"',
					]
				],
				[
					'saisie' => 'auteurs',
					'options' => [
						'nom' => 'expediteur_administrateur',
						'label' => _T('commandes:notifications_expediteur_administrateur_label'),
						'statut' => '0minirezo',
						'cacher_option_intro' => 'on',
						'defaut' => $config['expediteur_administrateur'],
						'afficher_si' => '@expediteur@ == "administrateur"',
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'expediteur_email',
						'label' => _T('commandes:notifications_expediteur_email_label'),
						'defaut' => $config['expediteur_email'],
						'afficher_si' => '@expediteur@ == "email"',
					]
				],
				[
					'saisie' => 'selection',
					'options' => [
						'nom' => 'vendeur',
						'label' => _T('commandes:notifications_vendeur_label'),
						'explication' => _T('commandes:notifications_vendeur_explication'),
						'cacher_option_intro' => 'on',
						'defaut' => $config['vendeur'],
						'data' => [
							'webmaster' => _T('commandes:notifications_vendeur_choix_webmaster'),
							'administrateur' => _T('commandes:notifications_vendeur_choix_administrateur'),
							'email' => _T('commandes:notifications_vendeur_choix_email')
						]
					]
				],
				[
					'saisie' => 'auteurs',
					'options' => [
						'nom' => 'vendeur_webmaster',
						'label' => _T('commandes:notifications_vendeur_webmaster_label'),
						'statut' => '0minirezo',
						'cacher_option_intro' => 'on',
						'webmestre' => 'oui',
						'multiple' => 'oui',
						'defaut' => $config['vendeur_webmaster'],
						'afficher_si' => '@vendeur@ == "webmaster"',
					]
				],
				[
					'saisie' => 'auteurs',
					'options' => [
						'nom' => 'vendeur_administrateur',
						'label' => _T('commandes:notifications_vendeur_administrateur_label'),
						'statut' => '0minirezo',
						'multiple' => 'oui',
						'cacher_option_intro' => 'on',
						'defaut' => $config['vendeur_administrateur'],
						'afficher_si' => '@vendeur@ == "administrateur"',
					]
				],

				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'vendeur_email',
						'label' => _T('commandes:notifications_vendeur_email_label'),
						'explication' => _T('commandes:notifications_vendeur_email_explication'),
						'defaut' => $config['vendeur_email'],
						'afficher_si' => '@vendeur@ == "email"',
					]
				],
				[
					'saisie' => 'oui_non',
					'options' => [
						'nom' => 'client',
						'label' => _T('commandes:notifications_client_label'),
						'explication' => _T('commandes:notifications_client_explication'),
						'defaut' => $config['client'],
					]
				]
			]
		];
	} else {
		$saisies[] = $notifications;
	}

	$saisies[] = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'fieldset_statuts_actifs_parametres',
			'label' => _T('commandes:titre_statuts_actifs_parametres'),
		],
		'saisies' => [
			[
				'saisie' => 'explication',
				'options' => [
					'nom' => 'explication_statuts_actifs',
					'texte' => _T('commandes:explication_statuts_actifs'),
				]
			],
			[
				'saisie' => 'oui_non',
				'options' => [
					'nom' => 'accueil_encours',
					'label' => _T('commandes:notifications_activer_label'),
					'explication' => _T('commandes:explication_accueil_encours'),
					'defaut' => isset($config['accueil_encours']) ? $config['accueil_encours'] : '',
				]
			],
			[
				'saisie' => 'checkbox',
				'options' => [
					'nom' => 'statuts_actifs',
					'label' => _T('commandes:label_statuts_actifs'),
					'data' => $statuts,
					'defaut' => isset($config['statuts_actifs']) ? $config['statuts_actifs'] : '',
					'explication' => _T('commandes:explication_choix_statuts_actifs'),
					'afficher_si' => '@accueil_encours@ == "on"'
				]
			]
		]
	];
	return $saisies;
}
