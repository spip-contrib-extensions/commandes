<?php

/**
 * Fonctions du plugin Commandes relatives à la référence de commande
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Fonctions
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Génère un numéro unique utilisé pour remplir le champ `reference` lors de la création d'une commande.
 * @deprecated
 * @uses commande_generer_reference_dist()
 *
 * @param int $id_commande
 * @param int $id_auteur
 *     (optionnel) identifiant de l'auteur
 * @return string
 *     reference de la commande
**/
function inc_commandes_reference_dist($id_commande, $id_auteur = 0) {

	// todo: trig deprecated
	$commande_generer_reference = charger_fonction('generer_reference', 'commande');
	return $commande_generer_reference($id_commande, $id_auteur);
}