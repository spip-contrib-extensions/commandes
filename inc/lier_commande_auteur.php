<?php

/**
 * Fonction du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 * Remplit le champ `id_auteur` d'une commande
 *
 * @deprecated
 * @uses commande_lier_auteur_dist()
 *
 * @param int $id_commande
 *     identifiant de la commande
 * @param int $id_auteur
 *     identifiant de l'auteur
 * @return mixed|string $err
 *     Message d'erreur éventuel
**/
function inc_lier_commande_auteur_dist($id_commande, $id_auteur) {

	// todo: trig deprecated
	$commande_lier_auteur = charger_fonction('lier_auteur', 'commande');
	return $commande_lier_auteur($id_commande, $id_auteur);
}