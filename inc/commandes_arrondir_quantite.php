<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) { return;
}

/**
 *
 * @deprecated
 * @uses commande_arrondir_quantite_dist()
 *
 * @return float
 *   Retourne la quantité arrondie
 */
function inc_commandes_arrondir_quantite_dist($quantite, $objet = '', $id_objet = 0) {

	// todo: trig deprecated
	$commande_arrondir_quantite = charger_fonction('arrondir_quantite', 'commande');
	return $commande_arrondir_quantite($quantite, $objet, $id_objet);
}