<?php

/**
 * Action du plugin Commandes
 *
 * @plugin     Commandes
 * @copyright  2014
 * @author     Ateliers CYM, Matthieu Marcillaud, Les Développements Durables
 * @licence    GPL 3
 * @package    SPIP\Commandes\Action
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Abandonne une commande
 *
 * @note
 * appel conventionnel par abandonner_transaction
 *
 * @example
 *     ```
 *     #URL_ACTION_AUTEUR{supprimer_commande,#ID_COMMANDE,#SELF}
 *     ```
 *
 * @param $arg string
 *     id_commande : identifiant de la commande
 * @return void
 */
function action_abandonner_commande_dist($id_commande = null) {

	// Si $arg n'est pas donné directement, le récupérer via _POST ou _GET
	if (is_null($id_commande)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$id_commande = $securiser_action();
	}

	// Passer le statut en abandonné
	if ($id_commande = intval($id_commande)) {
		include_spip('base/abstract_sql');
		spip_log("Commande $id_commande → abandon", 'commandes');
		sql_updateq(
			'spip_commandes',
			['statut' => 'abandonne'],
			'id_commande = ' . intval($id_commande)
		);
		// Retirer la commande si elle est en session
		include_spip('inc/session');
		if ($id_commande === intval(session_get('id_commande'))) {
			session_set('id_commande');
		}
	}
}
